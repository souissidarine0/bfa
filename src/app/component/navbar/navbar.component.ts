import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isCollapsed = false;
  constructor( private _router : Router) { }

  ngOnInit( ): void {
    // this._router.navigate( ['/somepath', id ], {fragment: 'test'});
    
  }

  openMenu(){
    this.isCollapsed = !this.isCollapsed;
    }
    goTo(location: string): void {
      window.location.hash = location;
  }
}
